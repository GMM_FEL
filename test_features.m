tic
[Visuals_train Boundary1_train Boundary2_train Filenames_train]=read_data('/home/tsix/tsix/high_jump/09sem/gmm/OCT/Lernstichprobe/');

[Visuals_test Boundary1_test Boundary2_test Filenames_test]=read_data('/home/tsix/tsix/high_jump/09sem/gmm/OCT/ttt1/');
[s1, S1, s2, S2] = get_probabilities(Visuals_train, Boundary1_train,Boundary2_train);
sigma1=10;
sigma2=40;

%for i=1:length(Visuals_test)
%for i=1:9
 for i=[34 24 9 8] 
    tic
  f1=extract_features1(Visuals_test{i}, 3);
  %toc
  %tic
  
  X=get_emission_probabilities(f1, s1,sigma1);
  %toc
  %tic
  fb_mean1=hmm_forward_backward(s1, S1, X,'mean');
  %toc
  %tic 

  f2=extract_features2(Visuals_test{i}, fb_mean1,s2,sigma2);
  %toc
  %tic 
  s=compose_priors(s2,fb_mean1); 
  %toc
  X=get_emission_probabilities(f2, s,sigma2);
  %toc
  %tic  
  fb_mean2=hmm_forward_backward(s(:,1), S2(1:size(s,1),1:size(s,1)), X,'mean');
  toc
  
  figure(i);
  show_result(Visuals_test{i}, fb_mean1,Boundary1_test{i},fb_mean2,Boundary2_test{i});
  
  %figure(i+3);
 %show_features(Visuals_test{i},f1,f2);
  [mse1 bias1 var1 count1 mse2 bias2 var2 count2]=validate(fb_mean1,Boundary1_test{i},fb_mean2,Boundary2_test{i});
  fprintf('Results for %s:\n',Filenames_test{i});
  fprintf('  Boundary1:\n');
  fprintf('    Bias=%f, MSE=%f, var=%f, size of ground truth: %d\n',bias1, mse1, var1, count1);
  fprintf('  Boundary2:\n');
  fprintf('    Bias=%f, MSE=%f, var=%f, size of ground truth: %d\n',bias2, mse2, var2, count2);
  fprintf('\n');
  
end




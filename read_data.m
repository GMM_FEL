% A script for reading the data into three variables:
%   Visuals - cell array containing matrices of observed data
%   Boundary1 - cell array containing vectors of height values for thwe
%   first boundary
%   Boundary2 - cell array containing vectors of height values for the
%   second boundary
%   Filenames - cell array containing names of loaded files

function[Visuals Boundary1 Boundary2 Filenames]=read_data(path_to_learnprobes)
%% Initialization

Visuals = cell(0);
Boundary1 = cell(0);
Boundary2 = cell(0);
Filenames = cell(0);


%% Execution
i = 1;
j = 1;
k = 1;
files = dir(path_to_learnprobes);
while k <= length(files)
        file = files(k);
        k = k + 1;
        if (file.isdir == 1)
                continue
        end
        if(isempty(findstr('grenzen', file.name)) && isempty(findstr('segmentiert', file.name)))
                Filenames{i}=file.name;
                Visuals{i} = imread(strcat(path_to_learnprobes, file.name));
                i = i + 1;
        else
                grenzen = uint8(rgb2gray(imread(strcat(path_to_learnprobes, file.name))));
                grenzen(grenzen~=255)=0;
                b1=-ones(1,size(grenzen,2));
                b2=-ones(1,size(grenzen,2));
                for l=1:size(grenzen,2)
                    
                  tmp=find(grenzen(:,l)<255);
                  for m=1:length(tmp)
                    if(m>1 && tmp(m)-tmp(m-1)>5)
                      b1(l)=mean(tmp(1:(m-1)));
                      break;
                    end
                  end
                  if(m==length(tmp)); b1(l)=mean(tmp); end
                  for n=length(tmp):-1:1
                    if(n<m-1); break; end
                    if(n<length(tmp) && tmp(n+1)-tmp(n)>5)
                      b2(l)=mean(tmp((n+1):end));
                      break;
                    end
                  end
                end
                Boundary1{j} = round(b1);
                Boundary2{j} = round(b2);
                j = j + 1;
        end
end

%% Cleanup
clear i j k file files path_to_learnprobes grenzen


function gmm_test()
% Just a few automated tests.

function test(num, asserted, obtained)
    if size(asserted) == size(obtained)
            r = max(abs(asserted - obtained)) > max(asserted)/1E5; % direct comparing is not good because of rounding errors
    else
            r = 1;
    end
    if r == 0
            fprintf(1, 'Test %i successfull.\n', num)
    else
            fprintf(1, 'FAIL - Test %i\n\tAsserted: ', num)
            disp(asserted');
            fprintf(1, '\n\tObtained:')
            disp(obtained');
    end
end


fprintf(1, '\nTesting the function hmm_viterbi...\n\n');
s = [0.5; 0.5];
S = [0.5 0.5; 0.5 0.5];
X = [0.9 0.1; 0.9 0.1; 0.9 0.1; 0.1 0.9; 0.1 0.9; 0.9 0.1];
test(1, [1 1 1 2 2 1]', hmm_viterbi(s, S, X));
       
s = [0.33; 0.33; 0.34];
S = [0.33 0.33 0.34; 0.33 0.33 0.34; 0.33 0.33 0.34;];
X = [0.8 0.1 0.1; 0.1 0.1 0.8; 0.1 0.1 0.8; 0.2 0.8 0.0];
test(2, [1 3 3 2]', hmm_viterbi(s, S, X));

s = [0.03; 0.03; 0.94];
S = [0.33 0.33 0.34; 0.33 0.33 0.34; 0.33 0.33 0.34;];
X = [0.8 0.1 0.1; 0.1 0.1 0.8; 0.1 0.1 0.8; 0.2 0.8 0.0];
test(3, [3 3 3 2]', hmm_viterbi(s, S, X));

s = [0.5; 0.5]; % what if more than one path is the most probable one?
S = [0.5 0.5; 0.5 0.5];
X = [0.5 0.5; 0.5 0.5;];
test(4, [1 1]', hmm_viterbi(s, S, X));

fprintf(1, '\nTesting the function hmm_forward_backward...\n\n');
s = [0.3; 0.3; 0.4];
S = [0.33 0.33 0.34; 0.33 0.33 0.34; 0.33 0.33 0.34;];
X = [0.8 0.1 0.1; 0.1 0.1 0.8; 0.1 0.1 0.8; 0.2 0.8 0.0];
test(5, [1 3 3 2]', hmm_forward_backward(s, S, X));

fprintf(1, '\nTesting the function smooth_x...\n\n');
X = [0.02; 0.58; 0.4];
X_ht = java.util.Hashtable;
X_ht.put(num2str([1]), 1);
X_ht.put(num2str([2]), 2);
X_ht.put(num2str([3]), 3);
test(6, [0.02*exp(0)+0.58*exp(-1/2)+0.4*exp(-4/2), 0.02*exp(-1/2)+0.58*exp(0)+0.4*exp(-1/2),0.02*exp(-4/2)+0.58*exp(-1/2)+0.4*exp(0)]', smooth_x(X, X_ht, 2, 'gaussian', 1));
 
end



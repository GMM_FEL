% get_prob - implementation of p(x,s)
%
% usage
%        [value index]=get_prob(x,features,sigma)
% input
%       s - natural number, a height value for which the probability is
%       computed
%       features - parameters of the underlying distribution - odd items
%       are mu_i, even are k_i
%       sigma - variance of a single part of the underlying distribution
%
% output
%       value - value=max{k_i*N(mu_i,sigma)} over all i's, where
%               N denotes normal distribution
%               k_i are even items of vector 'features',
%               mu_i are odd items of vector 'features' and
%               sigma is the variance of a single gaussian
%       index - index=argmax{k_i*N(mu_i,sigma)} over all i's, where all the
%       symbols have the same meaning as they have for the previous output
%       variable

function[value index]=get_prob(s,features,sigma)
%value=0;
%index=0;
%features1=features;
features=features(features>0);
if(isempty(features))
  index=0;
  value=1;  
else
    mu=features(1:2:end);
  k=features(2:2:end);
  ex=((s-mu).^2)/(2*sigma^2);
  tmp=log(k)-ex;
  index=find(tmp==max(tmp),1);
  %value=k(index)*(1/(sqrt(2*3.14159)*sigma))*exp(tmp(index));
  value=k(index)*(1/sigma)*exp(tmp(index));
end



%for i=1:2:length(features)
%  if(features(i+1)>0)
%    p=features(i+1)*normpdf(s,features(i),sigma);
    
%    if(p>value)
%      index=(i+1)/2;
%      value=p;
%    end
%  end
%end
%if(index==0)
%  index=find(abs(features(1:2:end)-s)==min(abs(features(1:2:end)-s)),1,'first');
%  value=1;
%end





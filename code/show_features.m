function show_features(scan, features1, features2)

assert(exist('scan', 'var') == 1, 'No original image supplied');

rgb = gray2rgb(scan);
for i=1:size(scan, 2)
        for j=1:2:size(features1,2)
            if(features1(i,j)>0)    
              rgb(features1(i,j), i, 1) = 0;
              rgb(features1(i,j), i, 2) = 255;
              rgb(features1(i,j), i, 3) = 0;
            end
        end
        for j=1:2:size(features2,2)
            if(features2(i,j)>0)    
              rgb(features2(i,j)+4, i, 1) = 255;
              rgb(features2(i,j)+4, i, 2) = 0;
              rgb(features2(i,j)+4, i, 3) = 0;
            end
        end
        
        
        
        
        
end

imshow(rgb);


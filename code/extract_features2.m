function X = extract_features2(visual_data, boundary1,s2,sigma)
% extract_features2 -  extracts features for the second boundary from 
%                      a single image
%
% usage
%       X = extract_features2(visual_data, boundary1, s2, sigma)
%
% input
%       visual_data - a single, grey-scaled image
%       boundary1 - a vector containign height values of the first boundary
%       at this picture (visual_data)
%       s2 - prior probabilities of the second boundary
%       sigma - a parameter of p(x,s).
%
% output
%       X - m x n sized matrix where m is dimensionality of the
%       feature space and n is the number of columns in the
%       original image
%
% description
%       Feature extraction
%
% author
%       Hynek Urban, urbanhyn@fel.cvut.cz


k1=[1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
    0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0;
    0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0;
    0 0 0 1 1 1 1 1 1 1 1 1 1 1 0 0 0;
    0 0 0 0 1 1 1 1 1 1 1 1 1 0 0 0 0;
    0 0 0 0 0 1 1 1 1 1 1 1 0 0 0 0 0;
    0 0 0 0 0 0 1 1 1 1 1 0 0 0 0 0 0;
    0 0 0 0 0 0 1 1 1 1 1 0 0 0 0 0 0;
    0 0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 0;
    0 0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 0;
    0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0];


k2=[0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0;
    0 0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 0;
    0 0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 0;
    0 0 0 0 0 0 1 1 1 1 1 0 0 0 0 0 0;
    0 0 0 0 0 0 1 1 1 1 1 0 0 0 0 0 0;
    0 0 0 0 0 1 1 1 1 1 1 1 0 0 0 0 0;
    0 0 0 0 1 1 1 1 1 1 1 1 1 0 0 0 0;
    0 0 0 1 1 1 1 1 1 1 1 1 1 1 0 0 0;
    0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0;
    0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0;
    1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1];
boundary1=boundary1+15;
im=double(visual_data);

im=im(1:450,:);
%aim=conv2(im,ones(7,13),'same');
%aim=aim-min(aim(:));
%aim=aim/max(aim(:));
im=conv2(im,[k1;-k2],'same');
%im=im.*aim;
X=-ones(length(boundary1),6);        

positions = myfindpeaks2D(im,0);
for i=1:size(im,2)

   locs=find(positions(:,i));
   locs=locs(locs>boundary1(i) & locs<boundary1(i)+length(s2));
   values=im(locs,i);
   [values order]=sort(values,'descend');
   locs=locs(order);
   if(~isempty(values))
     locs=locs(values/values(1)>0.1);
   end
   if(~isempty(locs))
     locs=[locs(1);locs(abs(locs-locs(1))>10)];
   end
  
    for j=1:min((size(X,2)/2),length(locs))
      X(i,j*2-1)=locs(j)-10;
      X(i,j*2)=im(locs(j),i)/im(locs(1),i);
    end
     
end

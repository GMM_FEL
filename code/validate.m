% validate - compute some basic statistics that estimates the errorness of
% the inferred boundaries
% usage
%       [mse1 bias1 count1 mse2 bias2 count2]=validate(boundary1,ground_truth1,boundary2,ground_truth2)
%
% input
%       boundary1 - a vector containing the inferred first boundary
%       ground_truth1 - a vector containing indices of the real first 
%       boundary loaded from a file
%       boundary2 - a vector containing the inferred second boundary
%       ground_truth2 - a vector containing indices of the real second 
%       boundary loaded from a file
% 
% output
%       mse1 - Mean square error for the first boundary
%       bias1 - Bias for the first boundary
%       count1 - Number of known indices of the real first boundary
%       mse2 - Mean square error for the second boundary
%       bias2 - Bias for the second boundary
%       count2 - Number of known indices of the real second boundary
%
function[mse1 bias1 sigma1 count1 mse2 bias2 sigma2 count2]=validate(boundary1,ground_truth1,boundary2,ground_truth2)
if(size(boundary1)~=size(ground_truth1)); boundary1=boundary1'; end
if(size(boundary2)~=size(ground_truth2)); boundary2=boundary2'; end

indices1=ground_truth1>1;
indices2=ground_truth2>1;
mse1=mean(abs(boundary1(indices1)-ground_truth1(indices1)));
mse2=mean(abs(boundary2(indices2)-ground_truth2(indices2)));
bias1=mean(boundary1(indices1)-ground_truth1(indices1));
bias2=mean(boundary2(indices2)-ground_truth2(indices2));
sigma1=var((boundary1(indices1)-ground_truth1(indices1))-bias1);
sigma2=var((boundary2(indices2)-ground_truth2(indices2))-bias2);
count1=sum(indices1);
count2=sum(indices2);



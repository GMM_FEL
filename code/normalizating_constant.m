% get_prob - computes normalizing constant for p(x,s)
%
% usage
%        c=normalizating_constant(sigma,features)
% input
%       features - parameters of the underlying distribution - odd items
%       are mu_i, even are k_i
%       sigma - variance of a single part of the underlying distribution
%
% output
%       c - such number, that integral of max{k_i*N(mu_i,sigma)} from -inf
%       to +inf is equal to 1

function[c]=normalizating_constant(sigma,features)
%c=1;
%return;
features=features(features~=-1);

[~,order] = sort(features(1:2:end));
crosses=zeros(1,2+sum(1:(length(features)/2-1)));
a=2;
for i=(order*2-1)
  for j=(order*2-1)
    if(i<j)
      crosses(a)=(features(j)*features(j)-features(i)*features(i)+2*sigma*sigma*log(features(i+1)/features(j+1)))/(2*(features(j)-features(i)));
      a=a+1;
    end
  end 
end
if(length(crosses)==2)
crosses(1)=0;
crosses(2)=1;
else
 crosses(1)=min(crosses(2:(end-1)))-1;
 crosses(end)=max(crosses(2:end-1))+1;   
    
end

crosses=sort(crosses);

s=0;
for i=1:length(crosses)
  if(i==1)
    [~, index]=get_prob(crosses(i),features,sigma);
    s=s+ features(index*2)*normcdf(crosses(i),features(index*2-1),sigma);
  elseif(i==length(crosses))
      [~, index]=get_prob(crosses(i),features,sigma);
       s=s+features(index*2)*(1-normcdf(crosses(i-1),features(index*2-1),sigma));
  else
      [~, index]=get_prob((crosses(i)+crosses(i-1))/2,features,sigma);
      s=s+features(index*2)*(normcdf(crosses(i),features(index*2-1),sigma)-normcdf(crosses(i-1),features(index*2-1),sigma));
  end

end

if(s==0); s=features(2); end
c=1/s;
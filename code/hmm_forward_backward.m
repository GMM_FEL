function ret = hmm_forward_backward(s, S, X, method)
% hmm_forward_backward - compute the sequence of the most probable hidden
% states given a sequence of observations
%
% usage
%       ret = hmm_forward_backward(s, S, X)
%
% input
%       s - prior probabilities for the first state (a column
%       vector)
%       S - transition probabilities between states (a
%       homogeneous HMM is assumed)
%       X - emission probabilities (rows correspond to
%       observations at given times, columns to hidden states)
%       method - if set to 'mean', computes the mean aposteriori
%       value, instead of MAP
% 
% output
%       ret - sequence of the most probable states
%
% description
%       An implementation of the Forward-Backward algorithm
%       inluding scaling to ensure numerical stability.
%       The alghorithm itself has been taken from Mr. Flach's
%       lectures, while the idea of scaling has been taken from
%       Kreich et al., 2009.
%
% author
%       Hynek Urban, urbanhyn@fel.cvut.cz

state_cnt = length(s);
seq_len = size(X, 1); 

% initialization
phis = zeros(size(X))';
psis = zeros(size(X))';
psis(:, end) = 1;
phis(:, 1) = s.*X(1, :)';

scale = zeros(1, seq_len);
scale(1) = sum(phis(:, 1));
phis(:, 1) = phis(:, 1)/scale(1);
% <--


% forward part
for i = 2:seq_len
        X_i = X(i, :);
        X_i = repmat(X_i, state_cnt ,1);
        X_i = X_i.*S; 
        phis(:, i) = (phis(:, i-1)'*X_i)';
        
        scale(i) = sum(phis(:, i)); % scaling
        phis(:, i) = phis(:, i)/scale(i);
end
 
% backward part
for i = (seq_len-1):-1:1
        X_i = X(i, :);
        X_i = repmat(X_i, state_cnt ,1);
        X_i = X_i.*S;
        psis(:, i) = X_i*psis(:, i+1);

        psis(:, i) = psis(:, i)/scale(i+1); % scaling
end


arr = phis.*psis;

% normalization
for i = 1:seq_len
        arr(:, i) = arr(:, i)/sum(arr(:, i));
end

if exist('method', 'var') && strcmp(method, 'mean')
    %fprintf(1, '    using mean aposteriori value instead of MAP\n');
    heights = 1:size(arr, 1);
    ret = heights*arr;
    ret = round(ret);
else
    [~, ret] = max(arr, [], 1); 
end

ret = ret';

end


function show_result(scan, boundary1, gt1, boundary2, gt2)
% Shows the scanned image with marked inferred boundaries.
%
% usage
%       show_result(scan, boundary1, boundary2)
% input
%       scan - the original greyscale scanned image
%       boundary1 - inferred sequence of height values for
%       boundary 1
%       boundary2 - inferred sequence of height values for
%       boundary 2
%
% output
%       none
%
% description
%       Shows the original scanned image with marked inferred
%       boundaries.
%
% author
%       Hynek Urban, urbanhyn@fel.cvut.cz

assert(exist('scan', 'var') == 1, 'No original image supplied');
if(~exist('boundary1', 'var')); boundary1=zeros(size(scan,2),1); end
if(~exist('boundary2', 'var')); boundary2=zeros(size(scan,2),1); end
if(~exist('gt1', 'var')); gt1=zeros(size(scan,2),1); end
if(~exist('gt2', 'var')); gt2=zeros(size(scan,2),1); end



rgb = gray2rgb(scan);
for i=1:size(scan, 2)
        if(boundary1(i)>1)
                rgb(boundary1(i), i, 1) = 0;
                rgb(boundary1(i), i, 2) = 255;
                rgb(boundary1(i), i, 3) = 0;
        end
        if(boundary2(i)>1)
                rgb(boundary2(i), i, 1) = 255;
                rgb(boundary2(i), i, 2) = 0;
                rgb(boundary2(i), i, 3) = 0;
        end
         if(gt1(i)>1)
                rgb(gt1(i), i, 1) = 0;
                rgb(gt1(i), i, 2) = 0;
                rgb(gt1(i), i, 3) = 255;
        end
        if(gt2(i)>1)
                rgb(gt2(i), i, 1) = 0;
                rgb(gt2(i), i, 2) = 0;
                rgb(gt2(i), i, 3) = 255;
        end       
        
        
        
        
        
        
end

imshow(rgb);


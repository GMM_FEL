%Useless method, but it takes only 0.03 s which makes it unworthy of removal
%(for sake of laziness)
function[ret]=compose_priors(s,boundary1)
ret=zeros(max(boundary1)+length(s),length(boundary1));
for i=1:length(boundary1)
  ret((boundary1(i)+1):(boundary1(i)+length(s)),i)=s;
end

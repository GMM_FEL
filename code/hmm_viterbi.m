function ret = hmm_viterbi(s, S, X)
% hmm_viterbi - compute the most likely sequence of hidden
% states given a sequence of observations
%
% usage
%       ret = hmm_viterbi(s, S, X)
%
% input
%       s - prior probabilities for the first state (a column
%       vector)
%       S - transition probabilities between states (a
%       homogeneous HMM is assumed)
%       X - emission probabilities (rows correspond to
%       observations at given times, columns to hidden states)
% 
% output
%       ret - the most probable sequence of states
%
% description
%       Implementation of the Viterbi algorithm, as described on
%       Wikipedia.
%
% author
%       Hynek Urban, urbanhyn@fel.cvut.cz

s = log(s);
S = log(S);
X = log(X+0.000001); % to ensure at least a rudimentary generalization ability
t = size(X, 1); % length of sequence
s_cnt = length(s); % number of possible states
pointers = zeros(s_cnt, t);
ret = zeros(t, 1);

V_0 = s+X(1, :)';

V_t = V_0;
for i=2:t
        V = repmat(V_t, 1, s_cnt);
        A = S+V;
        [Amax ind] = max(A, [], 1); % maximum over all states

        V_t = X(i, :)'+Amax';

        % store the "pointers"
        pointers(:, i-1) = ind';
        % <--
end

% finally use the "pointer" matrix to assemble the most probable
% sequence
[~, ret(t)] = max(V_t);
for i = t-1:-1:1
        ret(i) = pointers(ret(i+1), i);
end
% <--


end


% get_emission_probabilities - computes p(x,s) for a single image
%
% usage
%        X = get_emission_probabilities(features, s,sigma)
% input
%       features - a matrix of size(width(image),2*?) containing feature 
%       vectors for each column of a given image 
%       s - prior probabilities p(s)
%       sigma - variance of a single part of the underlying distribution
%
% output
%       X - emission probabilities (rows correspond to
%       observations at given times, columns to hidden states)
function [X] = get_emission_probabilities(features, s,sigma)
if(size(s,2)==1); s=repmat(s,1,size(features,1)); end
X = zeros(size(features,1),size(s,1));

for i=1:size(X,1)
    
  for j=1:size(X,2)
    if(s(j,i)==0)
      X(i,j)=0;
    else
      X(i,j)=get_prob(j,features(i,:),sigma);
    end
    
  end
end




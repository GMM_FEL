function ht = get_all_possible_x(method, range)
% get_all_possible_x - returns a hashtable, whose keys are all
% possible values of X, and whose values are IDs (or indices) of
% those specific values of X to be referenced by other methods
%
% usage
%        X = get_all_possible_x(method)
% input
%       method - the method used to extract features in
%       extract_features.m. Possible values are:
%           1: image is smoothed, features are coordinates of
%           two maximum horizontal differences
%       range - vector representing the domain of feature values
%       (assumed to be the same for all features)
%
% output
%       ht - hashtable, whose keys are all possible values of X,
%       and whose values are IDs (or indices - as inteded to be
%       used later) of the specific values of X.
%
% description
%       See section 'output'.
%
% author
%       Hynek Urban, urbanhyn@fel.cvut.cz


method_list = [1, 2];
assert(max(method_list == method) > 0, 'Unknown method code');


ht = java.util.Hashtable;
if (method == 1)
        id = 1;
        for i = range
                for j = range
                        key = num2str([i j]);
                        ht.put(key, id);
                        id = id + 1;
                end
        end

        return;
elseif (method == 2)
        id = 1;
        for i = range
                key = num2str([i]);
                ht.put(key, id);
                id = id + 1;
        end

        return;
end




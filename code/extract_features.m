function X = extract_features(visual_data, method)
% extract_features -  extracts features from a single image
%
% usage
%       X = extract_features(visual_data, method)
%
% input
%       visual_data - a single, grey-scaled image
%       method - code specifiying the method of feature
%       extraction. Possible values are:
%           1: image is smoothed, features are coordinates of
%           two maximum horizontal differences
%
% output
%       X - m x n sized matrix where m is dimensionality of the
%       feature space and n is the number of columns in the
%       original image
%
% description
%       Feature extraction
%
% author
%       Hynek Urban, urbanhyn@fel.cvut.cz


method_list = [1, 2];

assert(nargin > 1, 'Not enough arguments')
assert(max(method_list == method) > 0, 'Unknown method code');


if (method == 1)
        ret = zeros(2, size(visual_data, 2));

        % TODO - smoothing


        % conversion to differences
        vdata = visual_data(1:end-1, :);
        vdata_translated = visual_data(2:end, :);
        diff_data = vdata_translated - vdata;

        % finding the maximum indices
        [~, mind] = max(diff_data, [], 1);
        ret(1, :) = mind; % the first one
        for i=1:size(diff_data, 2) % for each column
                diff_data(mind(i), i) = -bitmax; % remove the maximum value
        end
        [~, mind] = max(diff_data, [], 1);
        ret(2, :) = mind; % the second one

        X = ret;
        return;
elseif(method==2)
        % blur image by a circular gaussian kernel
        f=fspecial('gaussian', 7, 3);
        f1=fspecial('disk', size(f,1)/2);
        f=f.*f1;
        visual_data = imfilter(visual_data, f);
        % compute vertical derivatives
        visual_data = abs(imfilter(visual_data, [-1; 0; 1]));
        % find global maxima for each column
        xx=(visual_data==repmat(max(visual_data),size(visual_data,1),1));
        X=zeros(1,size(visual_data,2));
        for i=1:size(xx,2)
          X(i)=find(xx(:,i),1)+2; % 2 is a Bulgarian constant, but it works
        end
        
        
end




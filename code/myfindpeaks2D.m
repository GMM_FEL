function[locs] = myfindpeaks2D(data,minpeaksvalue)
tmp1=conv2(data,[-1;1],'same');
tmp2=conv2(data,[1;-1],'same');
locs=[zeros(1,size(data,2));tmp1(2:(end-1),:)>=0 & tmp2(1:(end-2),:)>=0;zeros(1,size(data,2))];
locs=locs & data > minpeaksvalue;

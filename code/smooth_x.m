function ret = smooth_x(X, X_ht, fe_method, kernel_type, kernel_width)
% usage
%       ret =s mooth_x(X, X_ht, fe_method, kernel_type, kernel_width) 
%
% input
%       X - emission probabilities (as used inside
%       get_probabilities())
%       X_ht - hashtable as output from get_all_possible_x()
%       fe_method - code of the method used for feature
%       extraction
%       kernel_type - type of the kernel to smooth the
%       probabilities with. Possible choices are 'gaussian' or
%       'flat'.
%       kernel_width - width of the kernel to smooth the
%       probabilities with.
%
% output
%       X - m x n sized matrix where m is dimensionality of the
%       feature space and n is the number of columns in the
%       original image
%
% description
%       A helper function for histogram smoothing using various kernels.
%
% author
%       Hynek Urban, urbanhyn@fel.cvut.cz 

assert(nargin >= 5, 'Not enough input aruments');
assert(nargin < 6, 'Too many input aruments');
assert(strcmp(kernel_type, 'gaussian') || strcmp(kernel_type, 'flat'), 'Unknown kernel type');

if fe_method == 2

    ret = zeros(size(X));
    
    % Generally, we don't know whether neighbouring fids
    % correspond to neighbouring features. Therefore, the first
    % thing we have to do is rearrange all probability distributions
    % from X to somehow reflect spatial relationships
    
    % get the rearranged vector of all fids
    X_ht_ar = X_ht.entrySet.toArray;
    fids = zeros(length(X_ht_ar), 1);
    for i = 1:length(fids)
            fids(i) = X_ht.get(num2str(i));
    end
    % <--
    
    X_b = zeros(length(fids), 1);
    for b = 1:size(X, 2) % across all boundary values
        fprintf(1, '\r    smoothing: %i/%i', b, length(fids));
        X_b = X(fids, b); % extract histogram for the corresponding boundary value in condition
        
        % now, on the obtained histogram, do the smoothing itself:
        X_b_new = X_b;
        for j = 1:length(X_b)
                if strcmp(kernel_type, 'gaussian')
                        weights = (j - (1:length(X_b)));
                        weights = weights.^2; % squared euclidean distances
                        weights = exp(-weights/(2*kernel_width^2)); % gaussian kernel
                elseif strcmp(kernel_type, 'flat')
                        weights = abs(j - (1:length(X_b)));
                        weights(weights>kernel_width) = 0;
                        weigths(weights>0) = 1;
                end
                X_b_new(j) = weights*X_b;
        end
        % <--
        
        ret(:, b) = X_b_new;
    end

    fprintf(1, '\n');
end




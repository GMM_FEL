function [s1, S1,s2, S2] = get_probabilities(Visuals, Boundary1,Boundary2)
% get_probabilities - from the input data, compute the prior
% and transition probabilities for both boundaries and return them in the
% format required by the hmm_viterbi function
%
% usage
%       [s1, S1, s2, S2] = get_probabilities(Visuals, Boundary1, Boundary2)
%
% input
%       Visuals - a cell array containing matrices of visual
%       data, as obtained by running the read_data script and
%       consequently extracting features
%       Boundary1 - a cell array containing vectors of height
%       values of the first boundary, as obtained by the read_data script 
%       Boundary2 - a cell array containing vectors of height
%       values of the second boundary, as obtained by the read_data script

% output
%       s1 - prior probabilities for the first state of the first boundary
%       S1 - transition probabilities between states (heights) for the 
%       first boundary, rows correspond to target states, columns to the
%       starting ones
%       s2 - prior probabilities for the first state of the second boundary
%       S2 - transition probabilities between states (heights) for the 
%       second boundary, rows correspond to target states, columns to the
%       starting ones
%        S2(:, i) = S2(:, i)/sum(S2(:, i));

% description
%       Estimation of the model parameters given observed data
%       based on simple counting.
%       Missing data are ignored. The HMM is assumed to be both
%       homogeneous and (horizontally) translational invariant.
%
% author
%       Hynek Urban, urbanhyn@fel.cvut.cz

assert(length(Boundary1) == length(Visuals), 'Inconsistent data - feature & visuals counts mismatch');



%% Initialization
height=size(Visuals{1},1);
s1 = zeros(height, 1);%/height;
s2 = zeros(height, 1);%/height;

S1 = zeros(height);
S2 = zeros(height);



%% Computation - part 'S1'
maxbound1=0;
for i = 1:length(Boundary1)
        maxbound1=max(maxbound1,max(Boundary1{i}));
        boundary = Boundary1{i}(1:end-1);
        boundary_translated = Boundary1{i}(2:end);
        for j=1:length(boundary)
           if (boundary_translated(j) > 1 && boundary(j) > 1) % missing data
              s1(boundary(j))=s1(boundary(j))+1;
              for k=1-min(boundary_translated(j),boundary(j)):(height-max(boundary_translated(j),boundary(j)))
                
                  S1(boundary_translated(j)+k, boundary(j)+k) = S1(boundary_translated(j)+k, boundary(j)+k) + 1;
              end
           end
        end
end
s1(s1>0)=1;
s1(max(100,find(s1==1,1,'first')-100):find(s1==1,1,'first'))=1;
ubound=0.1;
for junk=1:100  % And normalize it
  for i=1:size(S1, 1)
        S1(:, i) = S1(:, i)/sum(S1(:, i));
  end
  if(isempty(find(S1>ubound,1)))
    break;
  else
    S1(S1>ubound)=ubound;
  end
end
%% Computation - part 'S2'
maxbound2=0;
for i = 1:length(Boundary2)
        boundary = Boundary2{i}-Boundary1{i};
        maxbound2=max(maxbound2,max(Boundary2{i}));
        if(boundary(1)>1 && Boundary1{i}(1)>1); s2(boundary(1))=s2(boundary(1))+1; end
        %if(Boundary2{i}(1)>1); s2(Boundary2{i}(1))=s2(Boundary2{i}(1))+1; end
        for j=2:length(boundary)
           if (boundary(j) > 1 && boundary(j-1) > 1 && Boundary1{i}(j) > 1 && Boundary1{i}(j-1)> 1) 
               s2(boundary(j))=s2(boundary(j))+1;
              for k=1-min(boundary(j),boundary(j-1)):(height-max(boundary(j),boundary(j-1)))
                  S2(boundary(j)+k, boundary(j-1)+k) = S2(boundary(j)+k, boundary(j-1)+k) + 1;
              end
           end
        end
end
ubound=0.1;
for junk=1:100  % And normalize it
  for i=1:size(S2, 1)
        S2(:, i) = S2(:, i)/sum(S2(:, i));
  end
  if(any(S2>ubound))
    S2(S2>ubound)=ubound;
  else
    break;  
  end
end



%s2(s2>0)=1;

%ubound=0.02;
%for junk=1:100  % And normalize it
%  s2=s2/sum(s2);
%  if(any(s2>ubound))
%    s2(s2>ubound)=ubound;
%  else
%    break;
%  end
%end

s2(s2>0)=1;
%%
s1=s1(1:maxbound1);
S1=S1(1:maxbound1,1:maxbound1);
s2=s2(1:find(s2>0,1,'last'));

%S2=S2(1:maxbound2,1:maxbound2);


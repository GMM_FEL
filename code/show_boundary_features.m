function show_boundary_features(scan, boundary1, features1, boundary2, features2)
% Shows the scanned image with marked inferred boundaries.
%
% usage
%       show_result(scan, boundary1, boundary2)
% input
%       scan - the original greyscale scanned image
%       boundary1 - inferred sequence of height values for
%       boundary 1
%       boundary2 - inferred sequence of height values for
%       boundary 2
%
% output
%       none
%
% description
%       Shows the original scanned image with marked inferred
%       boundaries.
%
% author
%       Hynek Urban, urbanhyn@fel.cvut.cz

assert(exist('scan', 'var') == 1, 'No original image supplied');

rgb = gray2rgb(scan);
for i=1:size(scan, 2)
        if exist('boundary1', 'var')
                if(boundary1(i)>1)
                  rgb(boundary1(i), i, 1) = 0;
                  rgb(boundary1(i), i, 2) = 255;
                  rgb(boundary1(i), i, 3) = 0;
                end
        end
        if exist('boundary2', 'var')
                if(boundary2(i)>1)
                  rgb(boundary2(i), i, 1) = 255;
                  rgb(boundary2(i), i, 2) = 0;
                  rgb(boundary2(i), i, 3) = 0;
                end
        end
        if exist('features1', 'var')
                j=find(abs(features1(i,1:2:end)-boundary1(i))==min(abs(features1(i,1:2:end)-boundary1(i))),1,'first');
                if(features1(i,j)>1)
                  rgb(features1(i,j), i, 1) = 255;
                  rgb(features1(i,j), i, 2) = 255;
                  rgb(features1(i,j), i, 3) = 255;
                end
        end
        
         if exist('features2', 'var')
                if(boundary2(i)>1)
                  j=find(abs(features2(i,1:2:end)-boundary2(i))==min(abs(features2(i,1:2:end)-boundary2(i))),1,'first')*2-1;
                  if(features2(i,j)>1)
                    if(j==1)
                      rgb(features2(i,j)+boundary1(i), i, 1) = 255;
                      rgb(features2(i,j)+boundary1(i), i, 2) = 255;
                      rgb(features2(i,j)+boundary1(i), i, 3) = 255;
                    else
                      rgb(features2(i,j)+boundary1(i), i, 1) = 0;
                      rgb(features2(i,j)+boundary1(i), i, 2) = 255;
                      rgb(features2(i,j)+boundary1(i), i, 3) = 0; 
                        
                    end
                  end
                else
                  j=find(features2(i,2:2:end)==max(features2(i,2:2:end)),1,'first')*2-1;
                  
                  if(features2(i,j)>1)
                    rgb(features2(i,j)+boundary1(i), i, 1) = 255;
                    rgb(features2(i,j)+boundary1(i), i, 2) = 255;
                    rgb(features2(i,j)+boundary1(i), i, 3) = 255;
                  end  
                    
                end
        end
               
end

imshow(rgb);


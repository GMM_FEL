% Crossvalidation to estimate the best parameters

clear all;

%% common part

FEATURE_EXTRACTION_METHOD = 2;

disp('Reading the data...');
read_data; % has to be configured first - see the code

% Set the range of potential boundary height values
min_height = 1;
max_height = size(Visuals{1}, 1);

disp('Extracting features...');
Features = cell(size(Visuals));
for i=1:length(Features)
        Features{i} = extract_features(Visuals{i}, FEATURE_EXTRACTION_METHOD);
end

disp('Generating the hashtable for all possible feature vectors... (this may take a while)');
X_ht = get_all_possible_x(FEATURE_EXTRACTION_METHOD, min_height:max_height);



%% <--

%% the crossvalidation part
kernel_types = {'gaussian', 'flat'};
kernel_widths = [0.02 0.1 0.5 1 2 4 7];
hmm_fb_methods = {'mean', 'map'};

for kt = 1:length(kernel_types)
        for kw = 1:length(kernel_widths)
                for fbm = 1:length(hmm_fb_methods)
                        fprintf(1, '====== Iteration for parameters (%s, %i, %s) ======\n', kernel_types{kt}, kernel_widths(kw), hmm_fb_methods{fbm});
                        disp('Computing model parameters...');
                        clear s S X;
                        [s, S, X] = get_probabilities(Features, Boundary1, X_ht, min_height, max_height, FEATURE_EXTRACTION_METHOD, kernel_types{kt}, kernel_widths(kw));

                        % Inference:
                        fim = Features{1};
                        true_sequence = Boundary1{1};


                        % create emission probabilities matrix as requested by
                        % hmm_viterbi()
                        X_viterbi = zeros(size(fim, 2), length(s));
                        for i=1:size(X_viterbi, 1)
                                fid = num2str(fim(:, i)'); % convert the feature to string -> feature id
                                X_viterbi(i, :) = X(X_ht.get(fid), :); % get this features' P(feature|state) for all states 
                        end
                        % <--
                        disp('Inferring boundary 1...');
                        inferred_sequence = hmm_forward_backward(s, S', X_viterbi, hmm_fb_methods{fbm});
                        
                        true_sequence_cleaned = true_sequence(true_sequence > 1); % remove missing data
                        inferred_sequence_cleaned = inferred_sequence(true_sequence > 1);

                        dist = sum(norm(inferred_sequence_cleaned - true_sequence_cleaned'));
                        fprintf(1, '====== Total euclidean distance between the true and the predicted sequence is: %f\n', dist);
                end
        end
end



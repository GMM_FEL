% An example showing the whole process of learning and inferring
% Boundary 1 - working, but highly incomplete

clear all;

FEATURE_EXTRACTION_METHOD = 2;

disp('Reading the data...');
read_data; % has to be configured first - see the code

% Set the range of potential boundary height values
min_height = 1;
max_height = size(Visuals{1}, 1);

disp('Extracting features...');
Features = cell(size(Visuals));
for i=1:length(Features)
        Features{i} = extract_features(Visuals{i}, FEATURE_EXTRACTION_METHOD);
end

disp('Generating the hashtable for all possible feature vectors... (this may take a while)');
X_ht = get_all_possible_x(FEATURE_EXTRACTION_METHOD, min_height:max_height);

clear s S X;
disp('Computing model parameters...');
[s, S, X] = get_probabilities(Features, Boundary1, X_ht, min_height, max_height, FEATURE_EXTRACTION_METHOD, 'gaussian', 1);


% Inference:
disp('Extracting features for a sample image...');
im = imread('/home/hynek/Media/Texty/Skola/GMM/OCT/Lernstichprobe/0183_B_01_05_Ru_6_SR830-i.png'); % edit
fim = extract_features(im, FEATURE_EXTRACTION_METHOD);
% create emission probabilities matrix as requested by
% hmm_viterbi()
X_viterbi = zeros(size(fim, 2), length(s));
for i=1:size(X_viterbi, 1)
        fid = num2str(fim(:, i)'); % convert the feature to string -> feature id
        X_viterbi(i, :) = X(X_ht.get(fid), :); % get this features' P(feature|state) for all states 
end
% <--
disp('Inferring boundary 1, at last...');
inferred_sequence = hmm_forward_backward(s, S', X_viterbi);

show_result(im, inferred_sequence);

